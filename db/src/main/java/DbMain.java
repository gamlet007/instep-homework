import java.sql.*;

public class DbMain {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/test","test", "test");

        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM account acc WHERE acc.firstname = 'ABC'");
        while (rs.next())
        {
            System.out.print("Column 1 returned ");
            System.out.println(rs.getString(1));
        }
        rs.close();
        st.close();
//close connection to database
        connection.close();
    }
}
